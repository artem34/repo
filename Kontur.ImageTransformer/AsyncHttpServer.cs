﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;

namespace Kontur.ImageTransformer
{
    internal class ImageTransform : IDisposable
    {
        public Rectangle Cropping(int x, int y, int w, int h, int PictW, int PictH)
        {
            int X = x, Y = y, W = w, H = h;


            //Обрезка параметров w и x
            {
                if ((x < 0) && (w > 0) && (w >= Math.Abs(x)))
                {
                    X = 0;
                    W -= Math.Abs(x);
                    W = (W > PictW) ? PictW : W;
                }
                if ((x >= 0) && (x <= PictW))
                {
                    if ((w < 0) && (Math.Abs(w) >= x))
                    {
                        W = x;
                        X = 0;
                    }
                    if ((w < 0) && (Math.Abs(w) < x))
                    {
                        X -= Math.Abs(w);
                        W = Math.Abs(w);
                    }
                    if ((w > 0) && (w > (PictW - x)))
                    {
                        W = PictW - x;
                    }
                }
                if ((x > PictW) && (w < 0))
                {
                    if ((x - Math.Abs(w) >= 0) && (x - Math.Abs(w) <= PictW))
                    {
                        X -= Math.Abs(w);
                        W = PictW - x;
                    }
                    if ((x - Math.Abs(w)) < 0)
                    {
                        X = 0;
                        W = PictW;
                    }
                }
            }
            //Обрезка параметров h и y
            if ((y < 0) && (h > 0) && (h >= Math.Abs(y)))
            {
                Y = 0;
                H -= Math.Abs(y);
                H = (H > PictH) ? PictH : H;
            }
            if ((y >= 0) && (y <= PictH))
            {
                if ((h < 0) && (Math.Abs(h) >= y))
                {
                    H = y;
                    Y = 0;
                }
                if ((h < 0) && (Math.Abs(h) < Y))
                {
                    Y -= Math.Abs(h);
                    H = Math.Abs(h);
                }
                if ((h > 0) && (h > (PictH - y)))
                {
                    H = PictH - y;
                }
            }
            if ((y > PictH) && (h < 0))
            {
                if ((y - Math.Abs(h) >= 0) && (y - Math.Abs(h) <= PictH))
                {
                    Y -= Math.Abs(h);
                    H = PictH - y;
                }
                if ((y - Math.Abs(h)) < 0)
                {
                    Y = 0;
                    H = PictH;
                }
            }
            return new Rectangle(X, Y, W, H);
        }
        public void Handling(HttpListenerContext listenerContext)
        {
            String req = listenerContext.Request.RawUrl;
            Stream body = listenerContext.Request.InputStream;
            bool badReq = false;
            //параметры x,y,w,h соответственно
            int[] coord = new int[4];
            Bitmap pict = null;
            int code = 204;
            try
            {
                badReq = true;
                if (listenerContext.Request.ContentLength64 > 102400)
                {
                    code = 400;
                    throw new Exception();        
                }

                pict = new Bitmap(body);
                if ((pict.Width > 1000) && (pict.Height > 1000))
                {
                    code = 400;
                    throw new Exception();
                }

                if (req.Contains("/process/rotate-cw/"))
                {
                    pict.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    badReq = false;
                }
                if (req.Contains("/process/rotate-ccw/"))
                {
                    pict.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    badReq = false;
                }
                if (req.Contains("/process/flip-h/"))
                {
                    pict.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    badReq = false;
                }
                if (req.Contains("/process/flip-v/"))
                {
                    pict.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    badReq = false;
                }
                if (badReq)
                {
                    code = 400;
                    throw new Exception();
                }

                string[] param = req.Split('/');
                string[] numbers = param[3].Split(',');
                for (int i = 0; i < 4; i++)
                {
                    if (!Int32.TryParse(numbers[i], out coord[i]))
                        throw new Exception();
                    if (Math.Abs(coord[i]) > Math.Pow(2, 31))
                        throw new Exception();
                }

                Rectangle cropRect = Cropping(coord[0], coord[1], coord[2], coord[3], pict.Width, pict.Height);
                pict = pict.Clone(cropRect, pict.PixelFormat);
            }
            catch (Exception e)
            {
                listenerContext.Response.StatusCode = code;
                listenerContext.Response.ContentLength64 = 0;
                badReq = true;
                using (Stream writer = listenerContext.Response.OutputStream)
                    writer.Write(new byte[] { }, 0, 0);
            }
            finally
            {
                if (!badReq)
                {
                    byte[] buffer = new ImageConverter().ConvertTo(pict, typeof(byte[])) as byte[];
                    listenerContext.Response.StatusCode = (int)HttpStatusCode.OK;
                    listenerContext.Response.ContentLength64 = buffer.Length;
                    listenerContext.Response.AddHeader("Content-Type", "application/octet-stream");

                    using (Stream writer = listenerContext.Response.OutputStream)
                        writer.Write(buffer, 0, buffer.Length);
                }
            }
        }
        public void TimeOut(object obj)
        {
            HttpListenerContext listenerContext = (HttpListenerContext)obj;
            listenerContext.Response.StatusCode = 429;
            listenerContext.Response.ContentLength64 = 0;

            using (Stream writer = listenerContext.Response.OutputStream)
                writer.Write(new byte[] { }, 0, 0);

        }
        public void Dispose()
        {

        }

    }

    internal class AsyncHttpServer : IDisposable
    {
        public AsyncHttpServer()
        {
            listener = new HttpListener();
        }

        public void Start(string prefix)
        {
            lock (listener)
            {
                if (!isRunning)
                {
                    listener.Prefixes.Clear();
                    listener.Prefixes.Add(prefix);
                    listener.Start();

                    listenerThread = new Thread(Listen)
                    {
                        IsBackground = true,
                        Priority = ThreadPriority.Highest
                    };
                    listenerThread.Start();

                    isRunning = true;
                }
            }
        }

        public void Stop()
        {
            lock (listener)
            {
                if (!isRunning)
                    return;

                listener.Stop();

                listenerThread.Abort();
                listenerThread.Join();

                isRunning = false;
            }
        }

        public void Dispose()
        {
            if (disposed)
                return;

            disposed = true;

            Stop();

            listener.Close();
        }

        private void Listen()
        {
            while (true)
            {
                try
                {
                    if (listener.IsListening)
                    {
                        var context = listener.GetContext();
                        Task.Run(() => HandleContextAsync(context));
                       // Console.WriteLine("New client");
                    }
                    else Thread.Sleep(0);
                }
                catch (ThreadAbortException)
                {
                    return;
                }
                catch (Exception error)
                {
                    // TODO: log errors
                }
            }
        }

        private async Task HandleContextAsync(HttpListenerContext listenerContext)
        {
            // TODO: implement request handling
            using (ImageTransform handle = new ImageTransform())
            {
                Task task = Task.Run(() => handle.Handling(listenerContext));
                bool result = await Task<bool>.Run(() =>
                {
                    /*if (!task.Wait(900))
                    {
                        Task.Run(() => handle.TimeOut(listenerContext));
                        task.Dispose();
                    }*/
                    return task.Wait(900);
                });
                if (!result)
                {
                    Task.Run(() => handle.TimeOut(listenerContext));
                    task.Dispose();
                }
                else task.Dispose();
            }
            //Console.Clear();
        }

        private readonly HttpListener listener;

        private Thread listenerThread;
        private bool disposed;
        private volatile bool isRunning;
    }
}